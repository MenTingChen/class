<?php
	require_once "conn.php";

	class ClassGetData {
		// 用userid去抓出對應的使用者的所有資訊
		public function GetUserInformation($userid){
			$result = DB::query("select * from user_information where user_id=%s",$userid);
			// echo json_encode($result);
			return json_encode($result);

		}

		//用userid去抓其組織資訊
		public function GetUserGroup($userid){
			//先抓出group_id
			$result1 = DB::query("select group_id from user_group where user_id= %d",$userid);
			foreach ($result1 as $key => $obj) {
				
				foreach ($obj as $key => $value) {
					$value = (int)$value;
					$result2 = DB::query("select * from `group` where group_id= %d",$value);
					return json_encode($result2);
				}
				
			}
			

		}
	}
?>